<?php
$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
session_start();
include 'constants.php';
$score = 0;

$cookie_duration = strtotime('+1 days');

$all_correct_answers = array();
$all_quest = array_merge($questions_page_1, $questions_page_2);
foreach($all_quest as $question){
    $all_correct_answers["ques_".$question->number] = $question->get_correct();
}

if($_POST){
    foreach($_POST as $key => $value) {
        setcookie($key, $value, $cookie_duration);
        if (in_array($key, array_keys($all_correct_answers))) {
            if ($_POST[$key] == $all_correct_answers[$key]) {
                $score++;
            }
            unset($all_correct_answers[$key]);
        }
    }
    header("Refresh:0; url=".$current_link);
};

foreach($_COOKIE as $key => $value) {
    if (in_array($key, array_keys($all_correct_answers))) {
        if ($_COOKIE[$key] == $all_correct_answers[$key]) {
            $score++;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles.css" />
    <form action="score.php" method="post">
    <title>Quizz</title>
</head>
<body>
<div class="container mt-sm-5 my-1 d-flex flex-column">
    <div class="h1 text-center" style="font-weight: 900">Your Score</div>
    <div class="h4 text-center" style="font-weight: 900"><?php echo $score?> / 10</div>
    <div class='h6 text-center' >
        <?php
        if ($score < 4) {
            echo 'Bạn quá kém, cần ôn tập thêm';
        } else if ($score >=4 && $score < 7){
            echo 'Cũng bình thường';
        } else {
            echo 'Sắp sửa làm được trợ giảng lớp PHP';
        }
        ?>
    </div>
</div>
<?php
        foreach($all_quest as $ques){
            echo "
            <div class='container mt-sm-5 my-1'>
                <div class='question ml-sm-5 pl-sm-5 pt-2'>
                    <div class='py-2 h5'>
                        <b>Question $ques->number. $ques->question</b>
                    </div>
            ";
            echo "<div class='ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3' id='options'>";
            for ($i=0 ; $i<=3 ; $i++){
                $q = $ques->answers[$i];
                $c = $ques->get_correct();
                $val = $i + 1;

                if (isset($_COOKIE['ques_'.$ques->number]) and $_COOKIE['ques_'.$ques->number] == $val) {
                    if ($val == $c) {
                        echo "<label class='options option-correct'> $q";
                    }
                    else {
                        echo "<label class='options option-incorrect'> $q";
                    }
                    echo "
                            <input type='checkbox' name='ques_$ques->number' disabled='disabled' value=$val checked>
                    ";
                } else {
                    if ($val == $c) {
                        echo "<label class='options option-correct'> $q
                                <input type='checkbox' name='ques_$ques->number' disabled='disabled' value=$val checked>
                        ";
                    }
                    else {
                        echo "<label class='options'> $q
                                <input type='checkbox' name='ques_$ques->number' disabled='disabled' value=$val>
                        ";
                    }
                }

                echo "      <span class='checkmark'></span>
                        </label>
                ";
            }
            echo "
                    </div>
                </div>
            </div>
            ";
        }
        ?>

</body>
</html>
